<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('about', function ()
{
    return view('about-us');
});
Route::get('board', function ()
{
    return view('board');
});
Route::get('departments', function()
{
  return view('departments');
});
Route::get('performance', function()
{
  return view('perfomance');
});


Route::get('register', function ()
{
    return view('register');
});


Route::get('bpost', function ()
{
    return view('blog/post');
});
Route::get('exams', function()
{
  return view('admin/exams');
});
Route::get('terms', 'TermsController@show');
Route::get('notice', function()
{
  return view('admin/notice');
});


Route::get('forms', function()
{
  return view('admin/forms');
});

Route::get('edit/{id}', 'TermsController@edit')->name('edit');
Route::post('/termdate', 'TermsController@store');
Route::delete('delete/{id}', 'TermsController@delete')->name('destroy');
Route::patch('update/{id}', 'TermsController@update')->name('update');
Route::get('admissions', 'AdmissionsController@show');
Route::get('admissionsedit/{id}', 'AdmissionsController@edit')->name('admissionsedit');
Route::post('/admissions', 'AdmissionsController@store');
Route::delete('admissionsdelete/{id}', 'AdmissionsController@delete')->name('admissionsdestroy');
Route::patch('admissionsupdate/{id}', 'AdmissionsController@update')->name('admissionsupdate');
Route::get('exams', 'ExamsController@show');
Route::get('examsedit/{id}', 'ExamsController@edit')->name('examsedit');
Route::post('/exams', 'ExamsController@store');
Route::delete('examsdelete/{id}', 'ExamsController@delete')->name('examsdestroy');
Route::patch('examsupdate/{id}', 'ExamsController@update')->name('examsupdate');
Route::get('announcements', 'AnnouncementsController@show');
Route::get('announcementsedit/{id}', 'AnnouncementsController@edit')->name('announcementsedit');
Route::post('/announcements', 'AnnouncementsController@store');
Route::delete('announcementsdelete/{id}', 'AnnouncementsController@delete')->name('announcementsdestroy');
Route::patch('announcementsupdate/{id}', 'AnnouncementsController@update')->name('announcementsupdate');
Route::post('/register', 'LoginController@store');
Route::get('admin', 'AdminController@show');
Route::patch('adminupdate/{id}', 'AdminController@update')->name('adminupdate');
Route::patch('admindecline/{id}', 'AdminController@decline')->name('admindecline');
Route::get('blog', 'BlogController@show');
Route::get('bcheck', 'LoginController@show');
Route::get('admincheck', 'LoginController@showadmin');
Route::post('blogin', 'LoginController@blogin');
Route::post('adminlogin', 'LoginController@adminlogin');
Route::post('post', 'PostsController@store');
Route::get('bhome', 'BlogController@index');
Route::get('bdetail/{id}','BlogController@detail' );
Route::get('schooladmissions', 'AdmissionsController@showschool');
Route::get('calendar','CalendarController@show');
Route::get('noticeboard', 'AnnouncementsController@shownotice');
Route::get('examsbank', 'ExamsController@showexams');
Route::get('/blogout', function() {

    // this is drawn from anvard's logout function
    //$hybridAuth = App::make('hybridauth');
    //Hybrid_Auth->logoutAllProviders();

    // this not working either :(
    // if(Auth::check()) {
    //     Auth::user()->remember_token = NULL;
    // }

    Auth::logout();
    Session::flush();
    return redirect('/')->with('status','Logged out');
});
Route::get('/adminlogout', function() {

    // this is drawn from anvard's logout function
    //$hybridAuth = App::make('hybridauth');
    //Hybrid_Auth->logoutAllProviders();

    // this not working either :(
    // if(Auth::check()) {
    //     Auth::user()->remember_token = NULL;
    // }

    Auth::logout();
    Session::flush();
    return redirect('admincheck')->with('status','Logged out');
});
