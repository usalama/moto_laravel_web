<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use App\Post;
use Intervention\Image\Facades\Image;
class PostsController extends Controller
{
  public function Store(Request $request)
  {
    if($request->hasFile('image_path'))
       {
         $file=$request->file('image_path');

         $filename=time(). '.' .$file->getClientOriginalExtension();
        $path=public_path('images/'. $filename);
         Image::make($file->getRealPath())->resize('669','320')->save($path);
          $databasePath='/images/'. $filename;

      $post = new Post;
      $post->image_path=$databasePath;
      $post->message=$request->message;

      if($post->save())
         {
           return redirect('blog')->with('status', 'Posted');
         }

}
      return redirect('blog')->with('status','Problem Saving');

  }
}
