<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Hash;
use App\User;
use Auth;
class LoginController extends Controller
{
  public function Show()
  {
     //$terms= Term::all();
     return view('login');
  }
  public function showadmin()
  {
     //$terms= Term::all();
     return view('adminlogin');
  }
  public function blogin(Request $request)
  {
    $user=array(
   'email'=>$request->email,
   'password'=>$request->password,
    'status'=>1
    );
    if (Auth::attempt($user,true))
  {
  	return redirect('blog');
  }
   return redirect('bcheck');

  }
  public function adminlogin(Request $request)
  {
    $user=array(
   'email'=>$request->email,
   'password'=>$request->password,
   'admin'=>1
    );
    if (Auth::attempt($user,true))
  {
  	return redirect('admin');
  }
   return redirect('admincheck');

  }
  public function Store(Request $request)
  {

      $user = new User;
      $user->name=$request->register;
      $user->email=$request->email;
      $user->password=Hash::make($request->pass);


      if($user->save())
         {
           Auth::loginUsingId($user->id);
          return redirect('bcheck')->with('status', 'Registered, Kindly wait for admin approval so that you can login');
           //return redirect('/register')->with('status', 'Registered, Kindly wait for admin approval so that you can login');
         }



  }
}
