<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Post;
class BlogController extends Controller
{
  public function Show()
  {
     //$terms= Term::all();
     if(Auth::check())
     {
     return view('blog/post');
   }
   else {
     return redirect('bcheck');
   }
  }
  public function index()
  {
     //$terms= Term::all();
    $posts=Post::paginate(8);

     return view('blog/index', ['posts'=>$posts]);

  }
  public function detail($id, Request $request )
  {
    $post=Post::findorFail($id);
    return view('blog/detail', ['post'=>$post]);
  }
}
