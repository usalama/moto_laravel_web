<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Term;
use Session;
use Image;

class TermsController extends Controller
{
    public function Store(Request $request)
    {
        $term = new Term;
        $term->termdate=$request->termdate;
        $term->enddate=$request->enddate;
        $term->description=$request->description;
        if($term->save())
           {
             return redirect('/terms')->with('status', 'Term dates Saved');
           }


    }
    public function Show()
    {
       $terms= Term::all();
       return view('admin/terms', ['terms'=>$terms]);
    }
    public function edit($id)
    {
      $term=Term::findorFail($id);
      //return redirect('edit')->with('offer', $offer);
      return view('admin/updateterm')->with('term', $term);

    }
    public function update ($id, Request $request)
   {
     $term= Term::findorFail($id);
     $term->description = $request->description;
     $term->enddate=$request->enddate;
     $term->termdate=$request->termdate;
     $term->save();
     Session::flash('status','Term date Updated');
     return redirect('terms');
     //return redirect()->back();
   }
    public function delete($id)
    {
      $term=Term::findorFail($id);
      $term->delete();
      Session::flash('status','Term date Deleted');
      return redirect('terms');
    }

}
