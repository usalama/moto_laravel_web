<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use Session;
class AdminController extends Controller
{
  public function Show()
  {
    if(Auth::check())
    {
    $users= User::all();
     return view('admin/index', ['users'=>$users]);
   }
   else {
     return redirect('admincheck');
   }
  }
  public function update ($id, Request $request)
 {
   $user= User::findorFail($id);
   $user->status = 1;

   $user->save();
   Session::flash('status','User Approved');
   return redirect('admin');
   //return redirect()->back();
 }
 public function decline ($id, Request $request)
{
  $user= User::findorFail($id);
  $user->status = 0;

  $user->save();
  Session::flash('status','User Rejected');
  return redirect('admin');
  //return redirect()->back();
}
}
