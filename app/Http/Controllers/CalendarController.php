<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Admission;
use App\Term;
use DateTime;
use Carbon;
use MaddHatter\LaravelFullcalendar\Event;
//use Calendar;
//use MaddHatter\LaravelFullcalendar\Calendar;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use EventModel;
class CalendarController extends Controller
{
  public function Show()
  {
    $events[] = \Calendar::event(
    'Event One', //event title
    false, //full day event?
    '2015-06-05T0800', //start time (you can also use Carbon instead of DateTime)
    '2015-06-05T0800', //end time (you can also use Carbon instead of DateTime)
    0 //optionally, you can specify an event ID
);

$eloquentEvents = Term::all(); //EventModel implements MaddHatter\LaravelFullcalendar\Event
 foreach($eloquentEvents as $eloquentEvent)
 {
$calendar = \Calendar::addEvents($events) //add an array with addEvents
    ->addEvent($eloquentEvent, [ //set custom color fo this event
        'color' => '#800',
    ])->setOptions([ //set fullcalendar options
        'firstDay' => 1
    ]);
}
     return view('calendar',compact('calendar'));
  }
}
