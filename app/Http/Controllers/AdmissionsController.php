<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admission;
use App\Http\Requests;
use Session;
class AdmissionsController extends Controller
{
  public function Store(Request $request)
  {
    if($request->hasFile('pdf_path'))
    {
      $file=$request->file('pdf_path');
      $path='uploads/';
      $extension= $file->getClientOriginalExtension();
      $file_name = rand(11111,99999).'.'.$extension;
      $file->move($path , $file_name);
      $databasePath=$path . $file_name;
      $admission = new Admission;
      $admission->pdf_path=$databasePath;
      $admission->title=$request->title;
      $admission->description=$request->description;
      if($admission->save())
         {
           return redirect('/admissions')->with('status', 'Info Saved');
         }
        return redirect('/admissions')->with('status', 'upload failed');
}

  }
  public function Show()
  {
     $admissions= Admission::all();
     return view('admin/admissions', ['admissions'=>$admissions]);
  }
  public function Showschool()
  {
     $admissions= Admission::all();
     return view('admissions', ['admissions'=>$admissions]);
  }
  public function edit($id)
  {
    $admission=Admission::findorFail($id);
    //return redirect('edit')->with('offer', $offer);
    return view('admin/updateadmissions')->with('admission', $admission);

  }
  public function update ($id, Request $request)
 {
   if($request->hasFile('pdf_path'))
   {
   $file=$request->file('pdf_path');
   $path='uploads/';
   $extension= $file->getClientOriginalExtension();
   $file_name = rand(11111,99999).'.'.$extension;
   $file->move($path , $file_name);
   $databasePath=$path . $file_name;
   $admission= Admission::findorFail($id);
   $admission->pdf_path=$databasePath;
   $admission->description = $request->description;
   $admission->title= $request->title;
   $admission->save();
   Session::flash('status','Info Updated');
   return redirect('admissions');
   //return redirect()->back();
 }
 else
   {
     $admission= Admission::findorFail($id);

     $admission->description = $request->description;
     $admission->title= $request->title;
     $admission->save();
     return redirect('/admissions')->with('status', 'no pdf selected for update');
   }


 }
  public function delete($id)
  {
    $admission=Admission::findorFail($id);
    $admission->delete();
    Session::flash('status','Info Deleted');
    return redirect('admissions');
  }
}
