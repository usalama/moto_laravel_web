<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Exam;

use Session;
class ExamsController extends Controller
{
  public function Store(Request $request)
  {
    if($request->hasFile('pdf_path'))
    {
      $file=$request->file('pdf_path');
      $path='uploads/';
      $extension= $file->getClientOriginalExtension();
      $file_name = rand(11111,99999).'.'.$extension;
      $file->move($path , $file_name);
      $databasePath=$path . $file_name;
      $exam = new Exam;
      $exam->pdf_path=$databasePath;
      $exam->title=$request->title;
      $exam->description=$request->description;
      if($exam->save())
         {
           return redirect('/exams')->with('status', 'Info Saved');
         }
        return redirect('/exams')->with('status', 'upload failed');
}

  }
  public function Show()
  {
     $exams= Exam::all();
     return view('admin/exams', ['exams'=>$exams]);
  }
  public function showexams()
  {
     $exams= Exam::paginate(10);
     return view('exampaper', ['exams'=>$exams]);
  }
  public function edit($id)
  {
    $exam=Exam::findorFail($id);
    //return redirect('edit')->with('offer', $offer);
    return view('admin/updateexams')->with('exam', $exam);

  }
  public function update ($id, Request $request)
 {
   if($request->hasFile('pdf_path'))
   {
   $file=$request->file('pdf_path');
   $path='uploads/';
   $extension= $file->getClientOriginalExtension();
   $file_name = rand(11111,99999).'.'.$extension;
   $file->move($path , $file_name);
   $databasePath=$path . $file_name;
   $exam= exam::findorFail($id);
   $exam->pdf_path=$databasePath;
   $exam->description = $request->description;
   $exam->title= $request->title;
   $exam->save();
   Session::flash('status','Info Updated');
   return redirect('exams');
   //return redirect()->back();
 }
 else
   {
     $exam= exam::findorFail($id);

     $exam->description = $request->description;
     $exam->title= $request->title;
     $exam->save();
     return redirect('/exams')->with('status', 'no pdf selected for update');
   }
 }
  public function delete($id)
  {
    $exam=Exam::findorFail($id);
    $exam->delete();
    Session::flash('status','Info Deleted');
    return redirect('exams');
  }
}
