<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Announcement;

use Session;
class AnnouncementsController extends Controller
{
  public function Store(Request $request)
  {


      $announcement = new Announcement;

      $announcement->title=$request->title;
      $announcement->description=$request->description;
      if($announcement->save())
         {
           return redirect('/announcements')->with('status', 'Info Saved');
         }



  }
  public function Show()
  {
     $announcement= Announcement::all();
     return view('admin/notice', ['announcements'=>$announcement]);
  }
  public function shownotice()
  {
     $announcement= Announcement::paginate(8);
     return view('noticeboard', ['announcements'=>$announcement]);
  }
  public function edit($id)
  {
    $announcement=Announcement::findorFail($id);
    //return redirect('edit')->with('offer', $offer);
    return view('admin/updateannouncements')->with('announcement', $announcement);

  }
  public function update ($id, Request $request)
 {

   $announcement= Announcement::findorFail($id);

   $announcement->description = $request->description;
   $announcement->title= $request->title;
   $announcement->save();
   Session::flash('status','Info Updated');
   return redirect('announcements');
   //return redirect()->back();

 return redirect('/announcements')->with('status', 'no pdf selected for update');
 }
  public function delete($id)
  {
    $announcement=Announcement::findorFail($id);
    $announcement->delete();
    Session::flash('status','Info Deleted');
    return redirect('announcements');
  }
}
