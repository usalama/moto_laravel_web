@extends('layouts.headerfooter')
@section('content')
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content">
                <div class="section background-opacity page-title set-height-top">
                    <div class="container">
                        <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">about us</h2>
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li class="active"><a href="#">About</a></li>
                            </ol>
                        </div>
                    </div>
                </div>

                <!--INTRO EDUGATE-->
                <div class="section intro-edu">
                    <div class="container">
                        <div class="intro-edu-wrapper">
                            <div class="row">
                                <div class="col-md-5"><img src="assets/images/people-avatar-8.png" alt="" class="intro-image fadeInLeft animated wow"/></div>
                                <div class="col-md-7">
                                    <div class="intro-title">WHO ARE <b>WE</b> ?</div>
                                    <div class="intro-content"><p>Moto secondary school was founded on 1 st Feb 2012 and is currently 4 years old. It was the brainchild of

the Moto Primary school committee. It started initially with 5 students, 1 TSC teacher and 3 teachers

employed by the SMC. The student population currently stands at 285 and continues to grow. It has 6

TSC employed teachers and 8 by the SMC. It also has 5 staff members in support capacities.</p>

                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section section-padding edu-ab">
                    <div class="container">
                        <div class="edu-ab">
                            <div class="group-title-index edu-ab-title"><h2 class="center-title">WHERE <b>ARE</b> WE LOCATED?</h2>
                            </div>
                            <div class="edu-ab-content">
                               <p>Moto Secondary is located in Molo Subcounty Molo Division, in a green, conducive learning

environment. It draws its student population from Molo town, Tanyai, Moto, Kapsita villages among

others.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section section-padding edu-feature">
                    <div class="container">
                        <div class="edu-feature-wrapper">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="group-title-index edu-feature-text"><h2 class="center-title">Social Economic Staus of The School</h2>

                                                <p class="top-title">Moto community draws its livelihood mainly from agricultural practices done in small scale capacities.

This gives a picture of a large population of locals on the lower income end of the scale.</p></div>
                                        </div>
                                      </div>
                                </div>
                                <div class="col-md-6"><img src="assets/images/iipingo.jpg" alt="" class="computer-image"/></div>
                            </div>
                        </div>
                    </div>
                </div>
               </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->
@endsection
