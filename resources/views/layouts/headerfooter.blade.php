@section('header')
<!DOCTYPE html>
<html lang="en">

<head><title>Moto Secondary School</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- LIBRARY FONT-->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,400italic,700,900,300">
    <link type="text/css" rel="stylesheet" href="/assets/font/font-icon/font-awesome-4.4.0/css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="/assets/font/font-icon/font-svg/css/Glyphter.css">
    <link type="text/css" rel="stylesheet" href="/css/fullcalendar.css">
    <link href='/css/fullcalendar.print.css' rel='stylesheet' media='print' />
    <!-- LIBRARY CSS-->
    <link type="text/css" rel="stylesheet" href="/assets/libs/animate/animate.css">
    <link type="text/css" rel="stylesheet" href="/assets/libs/bootstrap-3.3.5/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="/assets/libs/owl-carousel-2.0/assets/owl.carousel.css">
    <link type="text/css" rel="stylesheet" href="/assets/libs/selectbox/css/jquery.selectbox.css">
    <link type="text/css" rel="stylesheet" href="/assets/libs/fancybox/css/jquery.fancybox.css">
    <link type="text/css" rel="stylesheet" href="/assets/libs/fancybox/css/jquery.fancybox-buttons.css">
    <link type="text/css" rel="stylesheet" href="/assets/libs/media-element/build/mediaelementplayer.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.3/fullcalendar.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.3/fullcalendar.min.css"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.3/fullcalendar.print.css "></script>
    <!-- STYLE CSS    --><!--link(type="text/css", rel='stylesheet', href='assets/css/color-1.css', id="color-skins")-->
    <link type="text/css" rel="stylesheet" href="#" id="color-skins">
    <script src="/assets/libs/jquery/jquery-2.1.4.min.js"></script>
    <script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>
<script src='js/fullcalendar.min.js'></script>
    <script src="/assets/libs/js-cookie/js.cookie.js"></script>
    <script>if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1')) {
        $('#color-skins').attr('href', '/assets/css/' + Cookies.get('color-skin') + '.css');
    } else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1')) {
        $('#color-skins').attr('href', '/assets/css/color-1.css');
    }


    </script>
    <script>
    $(document).ready(function() {
      var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var output = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day;
		$('#calendar').fullCalendar({
			defaultDate: output,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
					title: 'No events',
					start: output
				}
			]
		});

	});
    </script>
</head>
<body><!-- HEADER-->
<header>
  <div class="header-topbar">
       <div class="container">
           <div class="topbar-left pull-left">
               <div class="email"><a href="#"><i class="topbar-icon fa fa-envelope-o"></i><span>hello.shinway@gmail.com</span></a></div>
               <div class="hotline"><a href="#"><i class="topbar-icon fa fa-phone"></i><span>+84 909 015 345</span></a></div>
           </div>
           <div class="topbar-right pull-right">
               <div class="socials"><a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="google"><i class="fa fa-google-plus"></i></a><a href="#" class="twitter"><i class="fa fa-twitter"></i></a><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a><a href="#" class="blog"><i class="fa fa-rss"></i></a><a href="#" class="dribbble"><i class="fa fa-dribbble"></i></a></div>
               <div class="group-sign-in"><a href="/bcheck" class="login">login</a><a href="register" class="register">register</a></div>
           </div>
       </div>
   </div>
    <div class="header-main homepage-01">
        <div class="container">
            <div class="header-main-wrapper">
                <div class="navbar-heade">
                    <div class="logo pull-left"><a href="index.html" class="header-logo"><img src="/assets/images/logo-color-1.png" alt=""/></a></div>
                    <button type="button" data-toggle="collapse" data-target=".navigation" class="navbar-toggle edugate-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <nav class="navigation collapse navbar-collapse pull-right">
                    <ul class="nav-links nav navbar-nav">
                                               <li class="active"><a href="/" class="main-menu">Home</a></li>
                                                <li><a href="about" class="main-menu">About</a></li>
                                                <li><a href="schooladmissions" class="main-menu">Admissions</a></li>
                                                <li><a href="board" class="main-menu">Board</a></li>
                                                <li><a href="performance" class="main-menu">Perfomance</a></li>
                                            <li><a href="blog" class="main-menu">Alumni</a></li>
<li class="dropdown"><a href="javascript:void(0)" class="main-menu">School<span class="fa fa-angle-down icons-dropdown"></span></a>
                            <ul class="dropdown-menu edugate-dropdown-menu-1">
                                                                <li><a href="/resources.html" class="link-page">Resources</a></li>

                                <li><a href="/calendar" class="link-page">Calendar</a></li>
                                <li><a href="/departments" class="link-page">Departments</a></li>
                                <li><a href="/noticeboard" class="link-page">Notice Board</a></li>
                                <li><a href="/examsbank" class="link-page">Exams Bank</a></li>
                            </ul>
                        </li>





                        <li><a href="/contact.html" class="main-menu">Contact Us</a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
@show
@yield('content')
@section('footer')
<footer>

  <div class="footer-main">
      <div class="container">
          <div class="footer-main-wrapper">
              <div class="row">
                  <div class="col-2">
                      <div class="col-md-3 col-sm-6 col-xs-6 sd380">
                          <div class="edugate-widget widget">
                              <div class="title-widget">Molo Secondary School</div>
                              <div class="content-widget"><p>Moto Secondary is located in Molo Subcounty Molo Division, in a green, conducive learning environment.  It draws its student population from Molo town, Tanyai, Moto, Kapsita villages among others.</p>

                                  <div class="info-list">
                                      <ul class="list-unstyled">
                                          <li><i class="fa fa-envelope-o"></i><a href="#">info@motosecschool.com</a></li>
                                          <li><i class="fa fa-phone"></i><a href="#">P: +254 709 456789</a></li>
                                          <li><i class="fa fa-map-marker"></i><a href="#"><p>Molo Town - Nakuru County</p>

                                              <p>Kenya - Rift Valley</p></a></li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-3 ">
                          <div class="useful-link-widget widget">
                              <div class="title-widget">USEFUL LINKS</div>
                              <div class="content-widget">
                                  <div class="useful-link-list">
                                      <div class="row">
                                          <div class="col-md-6 col-sm-6 col-xs-6">
                                              <ul class="list-unstyled">
                                                  <li><i class="fa fa-angle-right"></i><a href="index.html">Home</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="about-us.html">About</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="admissions.html">Admissions</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="board.html">Board</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="perfomance.html">Perfomance</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="alumni.html">Alumni</a></li>
                                              </ul>
                                          </div>
                                          <div class="col-md-6 col-sm-6 col-xs-6">
                                              <ul class="list-unstyled">
                                                  <li><i class="fa fa-angle-right"></i><a href="resources.html">Resources</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="calendar.html">Calendar</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="departments.html">Departments</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="noticeboard.html">Notice Board</a></li>
                                                  <li><i class="fa fa-angle-right"></i><a href="contact.html">Contact Us</a></li>
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

      </div>
  </div>
</footer>

<!-- LOADING-->
<div class="body-2 loading">
    <div class="dots-loader"></div>
</div>
<!-- JAVASCRIPT LIBS-->
<script>if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1')) {
    $('.logo .header-logo img').attr('src', 'assets/images/logo-' + Cookies.get('color-skin') + '.png');
} else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1')) {
    $('.logo .header-logo img').attr('src', 'assets/images/logo-color-1.png');
}</script>
<script src="/assets/libs/bootstrap-3.3.5/js/bootstrap.min.js"></script>
<script src="/assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
<script src="/assets/libs/owl-carousel-2.0/owl.carousel.min.js"></script>
<script src="/assets/libs/appear/jquery.appear.js"></script>
<script src="/assets/libs/count-to/jquery.countTo.js"></script>
<script src="/assets/libs/wow-js/wow.min.js"></script>
<script src="/assets/libs/selectbox/js/jquery.selectbox-0.2.min.js"></script>
<script src="/assets/libs/fancybox/js/jquery.fancybox.js"></script>
<script src="/assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
<!-- MAIN JS-->
<script src="/assets/js/main.js"></script>
<!-- LOADING SCRIPTS FOR PAGE-->
<script src="/assets/libs/isotope/isotope.pkgd.min.js"></script>
<script src="/assets/libs/isotope/fit-columns.js"></script>
<script src="/assets/js/pages/homepage.js"></script>
</body>

</html>
@show
