@extends('layouts.headerfooter')
@section('content')
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content"><!-- SLIDER BANNER-->
                <div class="section slider-banner set-height-top">
                    <div class="slider-item">
                        <div class="slider-1">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">Winning isn't everything</h5>

                                    <h1 class="text-info-1">IT'S THE ONLY THING</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-2">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">Welcome to Moto Secondary School</h5>

                                    <h1 class="text-info-1">SO WE BECOME WINNERS</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-3">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">You can download the admissions form below</h5>

                                    <h1 class="text-info-1">MOTO SECONDARY SCHOOL A SCHOOL WITH A CHANGE</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- TOP COURSES-->
                <div class="section section-padding top-courses">
                    <div class="container">
                        

                            <h2 class="center-title">EXAMS BANK</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                        </div>
                        <div class="top-courses-wrapper">
                          @foreach($exams as $exam)


                            <div class="top-courses-slider">
                                <div class="top-courses-item">
                                    <div class="edugate-layout-2">
                                        <div class="edugate-layout-2-wrapper">
                                            <div class="edugate-content"><a href="#" class="title">{!!$exam->title!!}</a>

                                                <div class="info">
                                                    <div class="author item"><a href="#">By Admin</a></div>
                                                    <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                                </div>

                                                <div class="description"><a href="{!!$exam->pdf_path!!}">{!!$exam->description!!}</a></div>

                                            </div>
                                            <div class="edugate-image"><img src="assets/images/courses/Untitled.png" alt="" class="img-responsive"/></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @endforeach
                           {!! $exams->links() !!}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->
@endsection
