<!DOCTYPE html>
<html lang="en">

<head><title>Moto Secondary School</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- LIBRARY FONT-->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,400italic,700,900,300">
    <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome-4.4.0/css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-svg/css/Glyphter.css">
    <!-- LIBRARY CSS-->
    <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
    <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap-3.3.5/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="assets/libs/owl-carousel-2.0/assets/owl.carousel.css">
    <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
    <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox.css">
    <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons.css">
    <link type="text/css" rel="stylesheet" href="assets/libs/media-element/build/mediaelementplayer.min.css">
    <!-- STYLE CSS    --><!--link(type="text/css", rel='stylesheet', href='assets/css/color-1.css', id="color-skins")-->
    <link type="text/css" rel="stylesheet" href="#" id="color-skins">
    <script src="assets/libs/jquery/jquery-2.1.4.min.js"></script>
    <script src="assets/libs/js-cookie/js.cookie.js"></script>
    <script>if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1')) {
        $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '.css');
    } else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1')) {
        $('#color-skins').attr('href', 'assets/css/color-1.css');
    }


    </script>
</head>
<body><!-- HEADER-->
<header>

    <div class="header-main homepage-01">
        <div class="container">
            <div class="header-main-wrapper">
                <div class="navbar-heade">
                    <div class="logo pull-left"><a href="index.html" class="header-logo"><img src="assets/images/logo-color-1.png" alt=""/></a></div>
                    <button type="button" data-toggle="collapse" data-target=".navigation" class="navbar-toggle edugate-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <nav class="navigation collapse navbar-collapse pull-right">
                    <ul class="nav-links nav navbar-nav">
                        <li ><a href="index.html" class="main-menu">Home</a></li>
                        <li ><a href="about-us.html" class="main-menu">About</a></li>
                        <li><a href="admissions.html" class="main-menu">Admissions</a></li>
                        <li ><a href="board.html" class="main-menu">Board</a></li>
                        <li><a href="perfomance.html" class="main-menu">Perfomance</a></li>
                        <li><a href="alumni.html" class="main-menu">Alumni</a></li>
                        <li class="dropdown active"><a href="javascript:void(0)" class="main-menu">School<span class="fa fa-angle-down icons-dropdown"></span></a>
                            <ul class="dropdown-menu edugate-dropdown-menu-1">
                                <li><a href="resources.html" class="link-page">Resources</a></li>

                                <li><a href="calendar.html" class="link-page">Calendar</a></li>
                                <li class="active"><a href="departments.html" class="link-page">Departments</a></li>
                                <li><a href="noticeboard.html" class="link-page">Notice Board</a></li>
                            </ul>
                        </li>





                        <li ><a href="contact.html" class="main-menu">Contact Us</a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content">
                <div class="section background-opacity page-title set-height-top">
                    <div class="container">
                        <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">Departments</h2>
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li class="active"><a href="#">Departments</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- CHOOSE COURSES-->
                <div class="section section-padding choose-course">
                    <div class="container">
                        <div class="group-title-index">

                            <h2 class="center-title">OUR DEPARTMENTS</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-a-1-01-01"></i></div>
                        </div>
                        <div class="choose-course-wrapper row">
                            <div class="col-md-4 col-xs-6">
                                <div class="item-course item-1">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-globe"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">English, Kiswahili,French & German</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Languages</a> </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="item-course item-2">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img fa fa-database"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Mathematics</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Mathematics</a> </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="item-course item-3">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-computer_network"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Chemistry,Physics & Biology</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Sciences</a> </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="item-course item-4">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-photocamera_1"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Geography, History & CRE</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Humanities</a> </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="item-course item-5">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-credit_card"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Homescience & Agriculture</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Technical and applied subjects</a> </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="item-course item-6">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-chat_bubbles"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Football, Volleybal & Handball</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Games</a> </div>
                                </div>
                            </div>
                             <div class="col-md-4 col-xs-6">
                                <div class="item-course item-6">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-chat_bubbles"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Journalism & Drama</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Clubs and societies</a> </div>
                                </div>
                            </div>
                             <div class="col-md-4 col-xs-6">
                                <div class="item-course item-6">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-chat_bubbles"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Guidance and counselling</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Guidance and counselling</a> </div>
                                </div>
                            </div>
                             <div class="col-md-4 col-xs-6">
                                <div class="item-course item-6">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-chat_bubbles"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Music and culture</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Music and culture</a> </div>
                                </div>
                            </div>
                             <div class="col-xs-6">
                                <div class="item-course item-6">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-chat_bubbles"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Examinations</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#"> Examinations</a> </div>
                                </div>
                            </div>
                             <div class="col-xs-6">
                                <div class="item-course item-6">
                                    <div class="icon-circle">
                                        <div class="icon-background"><i class="icons-img icon-chat_bubbles"></i></div>
                                        <div class="info">
                                            <div class="info-back"><a href="#">Welfare, environment and scouts</a></div>
                                        </div>
                                    </div>
                                    <div class="name-course"><a href="#">Welfare, environment and scouts</a> </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->
<footer>

    <div class="footer-main">
        <div class="container">
            <div class="footer-main-wrapper">
                <div class="row">
                    <div class="col-2">
                        <div class="col-md-3 col-sm-6 col-xs-6 sd380">
                            <div class="edugate-widget widget">
                                <div class="title-widget">Molo Secondary School</div>
                                <div class="content-widget"><p>Moto Secondary is located in Molo Subcounty Molo Division, in a green, conducive learning environment.  It draws its student population from Molo town, Tanyai, Moto, Kapsita villages among others.</p>

                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <li><i class="fa fa-envelope-o"></i><a href="#">info@motosecschool.com</a></li>
                                            <li><i class="fa fa-phone"></i><a href="#">P: +254 709 456789</a></li>
                                            <li><i class="fa fa-map-marker"></i><a href="#"><p>Molo Town - Nakuru County</p>

                                                <p>Kenya - Rift Valley</p></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="useful-link-widget widget">
                                <div class="title-widget">USEFUL LINKS</div>
                                <div class="content-widget">
                                    <div class="useful-link-list">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-angle-right"></i><a href="index.html">Home</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="about-us.html">About</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="admissions.html">Admissions</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="board.html">Board</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="perfomance.html">Perfomance</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="alumni.html">Alumni</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-angle-right"></i><a href="resources.html">Resources</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="calendar.html">Calendar</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="departments.html">Departments</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="noticeboard.html">Notice Board</a></li>
                                                    <li><i class="fa fa-angle-right"></i><a href="contact.html">Contact Us</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<!-- LOADING-->
<div class="body-2 loading">
    <div class="dots-loader"></div>
</div>
<!-- JAVASCRIPT LIBS-->
<script>if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1')) {
    $('.logo .header-logo img').attr('src', 'assets/images/logo-' + Cookies.get('color-skin') + '.png');
} else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1')) {
    $('.logo .header-logo img').attr('src', 'assets/images/logo-color-1.png');
}</script>
<script src="assets/libs/bootstrap-3.3.5/js/bootstrap.min.js"></script>
<script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
<script src="assets/libs/owl-carousel-2.0/owl.carousel.min.js"></script>
<script src="assets/libs/appear/jquery.appear.js"></script>
<script src="assets/libs/count-to/jquery.countTo.js"></script>
<script src="assets/libs/wow-js/wow.min.js"></script>
<script src="assets/libs/selectbox/js/jquery.selectbox-0.2.min.js"></script>
<script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
<script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
<!-- MAIN JS-->
<script src="assets/js/main.js"></script>
<!-- LOADING SCRIPTS FOR PAGE-->
<script src="assets/libs/isotope/isotope.pkgd.min.js"></script>
<script src="assets/libs/isotope/fit-columns.js"></script>
<script src="assets/js/pages/homepage.js"></script>
</body>

</html>