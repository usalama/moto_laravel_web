@extends('layouts.headerfooter')
@section('content')
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.3/fullcalendar.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.3/fullcalendar.min.css"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.3/fullcalendar.print.css "></script>
  <!-- STYLE CSS    --><!--link(type="text/css", rel='stylesheet', href='assets/css/color-1.css', id="color-skins")-->
  <link type="text/css" rel="stylesheet" href="#" id="color-skins">
  <script src="/assets/libs/jquery/jquery-2.1.4.min.js"></script>
  <script src='js/moment.min.js'></script>
<script src='js/jquery.min.js'></script>
<script src='js/fullcalendar.min.js'></script>
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content"><!-- SLIDER BANNER-->
                <div class="section slider-banner set-height-top">
                    <div class="slider-item">
                        <div class="slider-1">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">Winning isn't everything</h5>

                                    <h1 class="text-info-1">IT'S THE ONLY THING</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-2">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">We like to think we are winners</h5>

                                    <h1 class="text-info-1">SO WE BECOME WINNERS</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-3">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">Champions keep playing</h5>

                                    <h1 class="text-info-1">UNTIL THEY WIN</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- TOP COURSES-->
                <div class="section section-padding top-courses">
                    <div class="container">
                        <div class="group-title-index"><h4 class="top-title">WE DON'T JUST PARTICIPATE, WE WIN</h4>

                            <h2 class="center-title">OUR PERFOMANCES AND ACHIEVEMENTS</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                        </div>
                        @if(isset($calendar))
                        <h1> Calendar </h1>
    {!! $calendar->calendar() !!}
    {!! $calendar->script() !!}
  @endif
                  @if(!isset($calendar))
                    <div id="calendar"> </div>
                  @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->
@endsection
