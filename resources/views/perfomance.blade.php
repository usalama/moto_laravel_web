@extends('layouts.headerfooter')
@section('content')
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content"><!-- SLIDER BANNER-->
                <div class="section slider-banner set-height-top">
                    <div class="slider-item">
                        <div class="slider-1">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">Winning isn't everything</h5>

                                    <h1 class="text-info-1">IT'S THE ONLY THING</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-2">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">We like to think we are winners</h5>

                                    <h1 class="text-info-1">SO WE BECOME WINNERS</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-3">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">Champions keep playing</h5>

                                    <h1 class="text-info-1">UNTIL THEY WIN</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- TOP COURSES-->
                <div class="section section-padding top-courses">
                    <div class="container">
                        <div class="group-title-index"><h4 class="top-title">WE DON'T JUST PARTICIPATE, WE WIN</h4>

                            <h2 class="center-title">OUR PERFOMANCES AND ACHIEVEMENTS</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                        </div>
                        <div class="top-courses-wrapper">
                            <div class="top-courses-slider">
                                <div class="top-courses-item">
                                    <div class="edugate-layout-2">
                                        <div class="edugate-layout-2-wrapper">
                                            <div class="edugate-content"><a href="courses-detail.html" class="title">Music Festival 2015</a>

                                                <div class="info">
                                                    <div class="author item"><a href="#">By Admin</a></div>
                                                    <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                                </div>

                                                <div class="description">Yet again we won the 2015 music festival held in Meru. This is our 3rd title and not our last.</div>

                                            </div>
                                            <div class="edugate-image"><img src="assets/images/courses/Untitled.png" alt="" class="img-responsive"/></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top-courses-item">
                                    <div class="edugate-layout-2">
                                        <div class="edugate-layout-2-wrapper">
                                            <div class="edugate-content"><a href="courses-detail.html" class="title">KCSE Champions</a>

                                                <div class="info">
                                                    <div class="author item"><a href="#">By Admin</a></div>
                                                    <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                                </div>

                                                <div class="description">Yet again we are the national exams champions. This is our 3rd title and not our last.</div>
                                            </div>
                                            <div class="edugate-image"><img src="assets/images/courses/Untitled2.png" alt="" class="img-responsive"/></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="top-courses-slider">
                                <div class="top-courses-item">
                                    <div class="edugate-layout-2">
                                        <div class="edugate-layout-2-wrapper">
                                            <div class="edugate-content"><a href="courses-detail.html" class="title">Drama festivals</a>

                                                <div class="info">
                                                    <div class="author item"><a href="#">By Admin</a></div>
                                                    <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                                </div>

                                                <div class="description">Yet again we won the 2015 Drama festival held in Meru. This is our 3rd title and not our last.</div>
                                            </div>
                                            <div class="edugate-image"><img src="assets/images/courses/Untitled3.png" alt="" class="img-responsive"/></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top-courses-item">
                                    <div class="edugate-layout-2">
                                        <div class="edugate-layout-2-wrapper">
                                            <div class="edugate-content"><a href="courses-detail.html" class="title">Poetry and Dirges</a>

                                                <div class="info">
                                                    <div class="author item"><a href="#">By Admin</a></div>
                                                    <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                                </div>

                                                <div class="description">Yet again we won the 2015 Poetry and Dirges festival held in Meru. This is our 3rd title and not our last.</div>
                                            </div>
                                            <div class="edugate-image"><img src="assets/images/courses/Untitled4.png" alt="" class="img-responsive"/></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="group-btn-top-courses-slider">
                                <div class="btn-prev">&lsaquo;</div>
                                <div class="btn-next">&rsaquo;</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->
@endsection
