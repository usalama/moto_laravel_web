<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>

<!-- Mirrored from p.w3layouts.com/demos/personal_blog/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jun 2016 10:53:25 GMT -->
<head>
<title>Personal Blog a Blogging Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<link href="assets2/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="assets2/css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Personal Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates,  Android Compatible web template,
Smartphone Compatible web template, free web designs for Nokia, Samsung, LG, sony ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!----webfonts---->
<link href='http://fonts.googleapis.com/css?family=Oswald:100,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic' rel='stylesheet' type='text/css'>
<!----//webfonts---->
<script src="assets2/js/jquery.min.js"></script>
<!--end slider -->
<!--script-->
<script type="text/javascript" src="assets2/js/move-top.js"></script>
<script type="text/javascript" src="assets2/js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
<!---->
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');
</script>
<script async type='text/javascript' src='../../../../cdn.fancybar.net/ac/fancybar6a2f.js?zoneid=1502&amp;serve=C6ADVKE&amp;placement=w3layouts' id='_fancybar_js'></script>


<!---header---->
<div class="header">
	 <div class="container">
		  <div class="logo">
			  <a href="index.html"><img src="assets2/images/logo.jpg" title="" /></a>
		  </div>
			 <!---start-top-nav---->
			 <div class="top-menu">
				 <div class="search">
					 <form>
					 <input type="text" placeholder="" required="">
					 <input type="submit" value=""/>
					 </form>
				 </div>
				  <span class="menu"> </span>
				   <ul>
						<li class="active"><a href="bhome">HOME</a></li>

						<li><a href="bpost">POST</a></li>
						<li> </li>
            <li> </li>
						<li> </li>
						<li> </li>
						<li> </li>
						<li> </li>
						<li><a href="blogout">logout</a></li>
						<div class="clearfix"> </div>
				 </ul>
			 </div>
			 <div class="clearfix"></div>
					<script>
					$("span.menu").click(function(){
					$(".top-menu ul").slideToggle("slow" , function(){
					});
					});
					</script>
				<!---//End-top-nav---->

	 </div>
</div>

<div style="text-align: center;"><script async src="../../../../pagead2.googlesyndication.com/pagead/js/f.txt"></script>
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="6850850687"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
<div class="content">
	 <div class="container">
		 <div class="content-grids">
			 <div class="col-md-8 content-main">
				 <div class="content-grid">
					 @foreach($posts as $post)
					 <div class="content-grid-info">
						 <img src="{!!$post->image_path!!}" alt=""/>
						 <div class="post-info">
						 <h4><a href="{{ url('bdetail', $post->id)}}">Lorem ipsum dolor sit amet</a>  July 30, 2014 / 27 Comments</h4>
						 <p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis.</p>
						 <a href="{{ url('bdetail', $post->id)}}"><span></span>READ MORE</a>
						 </div>
					 </div>
				 @endforeach
           {!! $posts->links() !!}
<div style="text-align: center;"><script async src="../../../../pagead2.googlesyndication.com/pagead/js/f.txt"></script>
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="2281050280"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>


				 </div>
			  </div>
			  <div class="col-md-4 content-right">
				 <div class="recent">
					 <h3>RECENT POSTS</h3>
					 <ul>
					 <li><a href="#">Aliquam tincidunt mauris</a></li>
					 <li><a href="#">Vestibulum auctor dapibus  lipsum</a></li>
					 <li><a href="#">Nunc dignissim risus consecu</a></li>
					 <li><a href="#">Cras ornare tristiqu</a></li>
					 </ul>
				 </div>



		  </div>
	  </div>
</div>
<!---728x90--->
<div style="text-align: center;"><script async src="../../../../pagead2.googlesyndication.com/pagead/js/f.txt"></script>
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="6850850687"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
<!---->
<div class="footer">
	<div class="container">
		<p>© 2015 Personal Blog . All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
	</div>
</div>
