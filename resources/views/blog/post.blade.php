<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>

<!-- Mirrored from p.w3layouts.com/demos/personal_blog/web/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jun 2016 10:53:48 GMT -->
<head>
<title>Personal Blog a Blogging Category Flat Bootstrap Responsive Website Template | Contact :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Personal Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates,  Android Compatible web template,
Smartphone Compatible web template, free web designs for Nokia, Samsung, LG, sony ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!----webfonts---->
<link href='http://fonts.googleapis.com/css?family=Oswald:100,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic' rel='stylesheet' type='text/css'>
<!----//webfonts---->
<script src="js/jquery.min.js"></script>
<!--end slider -->
<!--script-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
<!---->

</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');
</script>
<script async type='text/javascript' src='../../../../cdn.fancybar.net/ac/fancybar6a2f.js?zoneid=1502&amp;serve=C6ADVKE&amp;placement=w3layouts' id='_fancybar_js'></script>


<!---header---->
<div class="header">
	 <div class="container">
		  <div class="logo">
			  <a href="blog"><img src="images/logo.jpg" title="" /></a>
		  </div>
			 <!---start-top-nav---->
			 <div class="top-menu">
				 <div class="search">
					 <form>
					 <input type="text" placeholder="" required="">
					 <input type="submit" value=""/>
					 </form>
				 </div>
				  <span class="menu"> </span>
				   <ul>
						<li><a href="bhome">HOME</a></li>

						<li class="active"><a href="bpost">POST</a></li>
						<li> </li>
            <li> </li>
						<li> </li>
						<li> </li>
						<li> </li>
						<li> </li>
						<li><a href="blogout">logout</a></li>
						<div class="clearfix"> </div>
				 </ul>
			 </div>
			 <div class="clearfix"></div>
					<script>
					$("span.menu").click(function(){
					$(".top-menu ul").slideToggle("slow" , function(){
					});
					});
					</script>
				<!---//End-top-nav---->
	 </div>
</div>
<!--/header-->

<div style="text-align: center;"><script async src="../../../../pagead2.googlesyndication.com/pagead/js/f.txt"></script>
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="6850850687"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
<div class="contact-content">
	 <div class="container">
		     <div class="contact-info">
			 <h2>POST</h2>
			 @if (Session::has('status'))
							 <div class="full_width confirmation_msg"> <span>{{ Session::get('status') }}</span> </div>
						 @endif
		     </div>
			 <div class="contact-details">
				 <form action="{{ url('post') }}" method="POST" enctype="multipart/form-data" >
		 			{!! csrf_field() !!}
				 <label class="btn btn-default btn-file">
                 Browse <input type="file" style="display: none;" name="image_path">
                </label>
                <br/>
                <br/>
				 <textarea placeholder="Message" name="message"></textarea>
				 <input type="submit" value="POST"/>
			 </form>
		  </div>

<div style="text-align: center;"><script async src="../../../../pagead2.googlesyndication.com/pagead/js/f.txt"></script>
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="6850850687"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
		  <div class="contact-details">
			  
			  <div class="col-md-6 company_address">
					<h4>GET IN TOUCH</h4>
					<p>Molo Town - Nakuru County,</p>
					<p>Kenya - Rift Valley,</p>

					<p>Phone:+254 709 456789</p>

					<p>Email: <a href="mailto:info@example.com">info@motosecschool.com</a></p>

			 </div>
			  <div class="clearfix"></div>
	     </div>


	</div>
</div>
<!---->

<div style="text-align: center;"><script async src="../../../../pagead2.googlesyndication.com/pagead/js/f.txt"></script>
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9153409599391170"
     data-ad-slot="6850850687"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
<div class="footer">
	<div class="container">
		<p>© 2015 Personal Blog . All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
	</div>
</div>
