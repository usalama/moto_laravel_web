@extends('layouts/_admin')
@section('content')

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Dashboard</li>
						
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">


						<div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											@if (Session::has('status'))
			                        <div class="full_width confirmation_msg"> <span>{{ Session::get('status') }}</span> </div>
			                      @endif
											<thead>
												<tr>

													<th class="detail-col">Name</th>
													<th>Email</th>


													<th>

														Status
													</th>
                          <th>

                          </th>


												</tr>
											</thead>

											<tbody>
                         @foreach($users as $user)
												<tr>


													<td>
                              {!!$user->name!!}
													</td>
													<td>{!!$user->email!!}</td>


													<td class="hidden-480">
														@if($user->status==0)
														<span class="label label-sm label-danger">not yet registered</span>
													@endif
													@if($user->status==1)
													<span class="label label-sm label-success">Registered</span>
												@endif
													</td>

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															{!! Form::open(['method' => 'PATCH','route' => ['adminupdate', $user->id]]) !!}
															<button class="btn btn-xs btn-success" type="submit">
																<i class="ace-icon fa fa-check bigger-120">

																</i>
															</button>
                              {!! Form::close() !!}
															<button class="btn btn-xs btn-info">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

															<button class="btn btn-xs btn-danger">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</button>
                             {!! Form::open(['method' => 'PATCH','route' => ['admindecline', $user->id]]) !!}
															<button class="btn btn-xs btn-warning">
																<i class="ace-icon fa fa-flag bigger-120"></i>
															</button>
															{!! Form::close() !!}
														</div>

														<div class="hidden-md hidden-lg">
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																</button>

																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																	<li>
																		<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																			<span class="blue">
																				<i class="ace-icon fa fa-search-plus bigger-120"></i>
																			</span>
																		</a>
																	</li>

																	<li>
																		<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>

																	<li>
																		<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																			<span class="red">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</td>
												</tr>

                    @endforeach
											</tbody>
										</table>
									</div><!-- /.span -->
								</div>




								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
@endsection
