@extends('layouts/_admin')
@section('content')
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Dashboard</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">


						<div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								@if (Session::has('status'))
												<div class="full_width confirmation_msg"> <span>{{ Session::get('status') }}</span> </div>
											@endif
                               <div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title">File Upload</h4>


												</div>
                                                 <form action="{{ url('announcements') }}" method="post" enctype="multipart/form-data">
																									 {!! csrf_field() !!}
												<div class="widget-body">
													<div class="widget-main">

													
														<div>
															<label for="form-field-mask-2">
																Title

															</label>

															<div >

																<input class="form-control " type="text" id="form-field" name="title" />
															</div>
														</div>

														<hr />
														<div>
															<label for="form-field-mask-3">
																Description

															</label>

															<div>


															<textarea class="form-control" id="form-field-8" placeholder="Default Text" name="description"></textarea>
														</div>
														</div>
                           <br/>

															 <button type="submit" value="proceed to next step" class="btn btn-success">submit</button>





													</div>

												</div>
											</div>

											</form>
										</div><!-- /.span -->



								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						<br/>
						<br/>
						<table id="simple-table" class="table  table-bordered table-hover">
							<thead>
								<tr>


									<th>Title</th>
									<th>Description</th>
									<th class="hidden-480">Date Uploaded</th>



									<th>Actions</th>
								</tr>
							</thead>

							<tbody>
								@foreach($announcements as $announcement)


								<tr>





									<td>{{$announcement->title}}</td>
									<td>{{$announcement->description}}</td>
									<td class="hidden-480">{{$announcement->created_at}}</td>
                 <td>
										<div class="hidden-sm hidden-xs btn-group">
											<button class="btn btn-xs btn-success">
												<i class="ace-icon fa fa-check bigger-120"></i>
											</button>

											<a href="{{ url('announcementsedit', $announcement->id)}}" class="btn btn-xs btn-info">
												<i class="ace-icon fa fa-pencil bigger-120"></i>
											</a>
                     {!! Form::open(['method' => 'DELETE','route' => ['announcementsdestroy', $announcement->id]]) !!}
										 {!! Form::submit('delete', ['class' => 'btn btn-xs btn-danger']) !!}

											{!! Form::close() !!}

											<button class="btn btn-xs btn-warning">
												<i class="ace-icon fa fa-flag bigger-120"></i>
											</button>
										</div>

										<div class="hidden-md hidden-lg">
											<div class="inline pos-rel">
												<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
													<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
												</button>

												<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
													<li>
														<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
															<span class="blue">
																<i class="ace-icon fa fa-search-plus bigger-120"></i>
															</span>
														</a>
													</li>

													<li>
														<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
															<span class="green">
																<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
															</span>
														</a>
													</li>

													<li>
														<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
															<span class="red">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
              @endforeach

							</tbody>
						</table>
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
@endsection
