@extends('layouts/_admin')
@section('content')
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Dashboard</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">


						<div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

											{!! Form::model($announcement,array('method' => 'PATCH', 'enctype'=>'multipart/form-data','route'=>array('announcementsupdate',$announcement->id)))!!}

									{!! csrf_field() !!}
								<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title">Term Dates</h4>

													<span class="widget-toolbar">
														<a href="#" data-action="settings">
															<i class="ace-icon fa fa-cog"></i>
														</a>

														<a href="#" data-action="reload">
															<i class="ace-icon fa fa-refresh"></i>
														</a>

														<a href="#" data-action="collapse">
															<i class="ace-icon fa fa-chevron-up"></i>
														</a>

														<a href="#" data-action="close">
															<i class="ace-icon fa fa-times"></i>
														</a>
													</span>
												</div>

												<div class="widget-body">
													<div class="widget-main">

														<div>
															<label for="form-field-mask-2">
																Title

															</label>

															<div>

																<input class="form-control " type="text" id="form-field" name="title" value="{!!$announcement->title!!}" />
															</div>
														</div>



														<hr />

														<div>
															<label for="form-field-8">Description</label>

															<textarea class="form-control" id="form-field-8" placeholder="" name="description">{!!$announcement->description!!}</textarea>
														</div>
														<br />
                           <button type="submit" value="proceed to next step" class="btn btn-success">submit</button>
													</div>
												</div>
											</div>
										</div><!-- /.span -->

                 {!! Form::close() !!}



								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						<br />
						<br />

					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		@endsection
