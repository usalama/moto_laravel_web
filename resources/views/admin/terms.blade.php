@extends('layouts/_admin')
@section('content')
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Dashboard</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">


						<div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								@if (Session::has('status'))
                        <div class="full_width confirmation_msg"> <span>{{ Session::get('status') }}</span> </div>
                      @endif
								<form action="{{ url('termdate') }}" method="POST" >
									{!! csrf_field() !!}
								<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title">Term Dates</h4>

													<span class="widget-toolbar">
														<a href="#" data-action="settings">
															<i class="ace-icon fa fa-cog"></i>
														</a>

														<a href="#" data-action="reload">
															<i class="ace-icon fa fa-refresh"></i>
														</a>

														<a href="#" data-action="collapse">
															<i class="ace-icon fa fa-chevron-up"></i>
														</a>

														<a href="#" data-action="close">
															<i class="ace-icon fa fa-times"></i>
														</a>
													</span>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div>
															<label for="id-date-picker-1">Start Date</label>

														<div class="row">
															<div class="col-xs-8 col-sm-11">
																<div class="input-group">
																	<input class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" name="termdate"/>
																	<span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>
																</div>
															</div>
														</div>
														</div>



														<hr />
														<div>
 														 <label for="id-date-picker-1">End Date</label>

 													 <div class="row">
 														 <div class="col-xs-8 col-sm-11">
 															 <div class="input-group">
 																 <input class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" name="enddate"/>
 																 <span class="input-group-addon">
 																	 <i class="fa fa-calendar bigger-110"></i>
 																 </span>
 															 </div>
 														 </div>
 													 </div>
 													 </div>
                              <hr />
														<div>
															<label for="form-field-8">Description</label>

															<textarea class="form-control" id="form-field-8" placeholder="Default Text" name="description"></textarea>
														</div>
														<br />
                           <button type="submit" value="proceed to next step" class="btn btn-success">submit</button>
													</div>
												</div>
											</div>
										</div><!-- /.span -->

                 </form>



								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						<br />
						<br />
						<table id="simple-table" class="table  table-bordered table-hover">
							<thead>
								<tr>


									<th>Start Date</th>
									<th> End Date </th>
									<th>Description</th>
									<th class="hidden-480">Date Uploaded</th>



									<th>Actions</th>
								</tr>
							</thead>

							<tbody>
								@foreach($terms as $term)


								<tr>




									<td>
										{{$term->termdate}}
									</td>
									<td>
										{{$term->enddate}}
									</td>
									<td>{{$term->description}}</td>
									<td class="hidden-480">{{$term->created_at}}</td>
                 <td>
										<div class="hidden-sm hidden-xs btn-group">
											<button class="btn btn-xs btn-success">
												<i class="ace-icon fa fa-check bigger-120"></i>
											</button>

											<a href="{{ url('edit', $term->id)}}" class="btn btn-xs btn-info">
												<i class="ace-icon fa fa-pencil bigger-120"></i>
											</a>
                     {!! Form::open(['method' => 'DELETE','route' => ['destroy', $term->id]]) !!}
										 {!! Form::submit('delete', ['class' => 'btn btn-xs btn-danger']) !!}

											{!! Form::close() !!}

											<button class="btn btn-xs btn-warning">
												<i class="ace-icon fa fa-flag bigger-120"></i>
											</button>
										</div>

										<div class="hidden-md hidden-lg">
											<div class="inline pos-rel">
												<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
													<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
												</button>

												<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
													<li>
														<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
															<span class="blue">
																<i class="ace-icon fa fa-search-plus bigger-120"></i>
															</span>
														</a>
													</li>

													<li>
														<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
															<span class="green">
																<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
															</span>
														</a>
													</li>

													<li>
														<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
															<span class="red">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
              @endforeach

							</tbody>
						</table>
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		@endsection
