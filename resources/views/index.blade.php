@extends('layouts.headerfooter')
@section('content')
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
          @if (Session::has('status'))
                  <div class="full_width confirmation_msg"> <span>{{ Session::get('status') }}</span> </div>
                @endif
            <div class="content"><!-- SLIDER BANNER-->
                <div class="section slider-banner set-height-top">
                    <div class="slider-item">
                        <div class="slider-1">
                            <div class="slider-caption">
                                <div class="container"><h5 class="text-info-2">Our Motto</h5>

                                    <h1 class="text-info-1">Hardwork pays</h1>


                                    <button class="btn btn-green"><span>Apply now !</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-2">
                            <div class="slider-caption">
                              <div class="container"><h5 class="text-info-2">Our Vision</h5>

                                  <h1 class="text-info-1">To be a great school</h1>


                                  <button class="btn btn-green"><span>Apply now !</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="slider-3">
                            <div class="slider-caption">
                              <div class="container"><h5 class="text-info-2">Our Mission</h5>

                                  <h1 class="text-info-1">To nurture and develop</h1>


                                  <button class="btn btn-green"><span>Apply now !</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                            <!--INTRO EDUGATE-->
                <div class="section intro-edu">
                    <div class="container">

                        <div class="intro-edu-wrapper">
                            <div class="row">
                                <div class="col-md-5"><img src="assets/images/people-avatar-8.png" alt="" class="intro-image fadeInLeft animated wow"/></div>
                                <div class="col-md-7">
                                    <div class="intro-title"><b>Moto Secondary School </b> </div>
                                    <div class="intro-content"><p> Moto Secondary Schoolis situated on the Molo-Olenguruone road about 2km from the town. The

school is under the leadership of the school principal Mrs. Priscah Ndungu. It currently has a student

                                        population of 285 students, 12 teachers and 5 support staff.</p>

<p> Download our admissions form: admissions page link</p></div>

                                </div>
                            </div>
                        </div>
                        </div>
                    </div>

                <!-- PROGRESS BARS-->
                <div class="section progress-bars section-padding">
                    <div class="container">
                        <div class="progress-bars-content">
                            <div class="progress-bar-wrapper"><h2 class="title-2">Some important facts about us</h2>

                                <div class="row">
                                    <div class="content">
                                        <div class="col-sm-4">
                                            <div class="progress-bar-number">
                                                <div data-from="0" data-to="12" data-speed="1000" class="num">0</div>
                                                <p class="name-inner">teachers</p></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="progress-bar-number">
                                                <div data-from="0" data-to="285" data-speed="1000" class="num">0</div>
                                                <p class="name-inner">students</p></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="progress-bar-number">
                                                <div data-from="0" data-to="12" data-speed="1000" class="num">0</div>
                                                <p class="name-inner">support staff</p></div>
                                        </div>

                                    </div>
                                </div>

                                <div class="group-btn-slider">
                                    <div class="btn-prev"><i class="fa fa-angle-left"></i></div>
                                    <div class="btn-next"><i class="fa fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   <!-- BEST STAFF-->
                <div class="section section-padding background-opacity best-staff">
                    <div class="container">
                        <div class="group-title-index"><h4 class="top-title">touch them if you want</h4>

                            <h2 class="center-title">learn from the best</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-icon-05"></i></div>
                        </div>
                        <div class="best-staff-wrapper">
                            <div class="best-staff-content">
                                <div class="staff-item">
                                    <div class="staff-item-wrapper">
                                        <div class="staff-info"><a href="#" class="staff-avatar"><img src="assets/images/people-avatar-2.jpg" alt="" class="img-responsive"/></a><a href="#" class="staff-name">Alex trevor</a>

                                            <div class="staff-job">head teacher</div>
                                            <div class="staff-desctiption">Nam libelo tempore, cum soluta nobis est eligendi optio cumque nilhi impedil quo minus end maximie fade posimus the end.</div>
                                        </div>
                                    </div>
                                    <div class="staff-socials"><a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="google"><i class="fa fa-google-plus"></i></a><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></div>
                                </div>
                                <div class="staff-item">
                                    <div class="staff-item-wrapper">
                                        <div class="staff-info"><a href="#" class="staff-avatar"><img src="assets/images/people-avatar-3.jpg" alt="" class="img-responsive"/></a><a href="#" class="staff-name">lana simth</a>

                                            <div class="staff-job">vice head teacher</div>
                                            <div class="staff-desctiption">Nam libelo tempore, cum soluta nobis est eligendi optio cumque nilhi impedil quo minus end maximie fade posimus the end.</div>
                                        </div>
                                    </div>
                                    <div class="staff-socials"><a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="google"><i class="fa fa-google-plus"></i></a><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></div>
                                </div>
                                <div class="staff-item">
                                    <div class="staff-item-wrapper">
                                        <div class="staff-info"><a href="#" class="staff-avatar"><img src="assets/images/people-avatar-4.jpg" alt="" class="img-responsive"/></a><a href="#" class="staff-name">barry join</a>

                                            <div class="staff-job">advisory professor</div>
                                            <div class="staff-desctiption">Nam libelo tempore, cum soluta nobis est eligendi optio cumque nilhi impedil quo minus end maximie fade posimus the end.</div>
                                        </div>
                                    </div>
                                    <div class="staff-socials"><a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="google"><i class="fa fa-google-plus"></i></a><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></div>
                                </div>
                                <div class="staff-item">
                                    <div class="staff-item-wrapper">
                                        <div class="staff-info"><a href="#" class="staff-avatar"><img src="assets/images/people-avatar-5.jpg" alt="" class="img-responsive"/></a><a href="#" class="staff-name">Ven Tomarme</a>

                                            <div class="staff-job">Design teacher</div>
                                            <div class="staff-desctiption">Nam libelo tempore, cum soluta nobis est eligendi optio cumque nilhi impedil quo minus end maximie fade posimus the end.</div>
                                        </div>
                                    </div>
                                    <div class="staff-socials"><a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="google"><i class="fa fa-google-plus"></i></a><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                      <br>
                    <div class="group-button">
                     <button class="btn btn-green btn-latest-new"><span>More Staff<i class="fa fa-long-arrow-right"></i></span></button>
                        </div>
                </div>

                 <!--LATEST NEW-->
                <div class="section section-padding latest-news">
                    <div class="container">
                        <div class="group-title-index"><h4 class="top-title">latest news</h4>

                            <h2 class="center-title">all about edugate updates</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-a-01-01"></i></div>
                        </div>
                        <div class="latest-news-wrapper">
                            <div class="edugate-layout-1">
                                <div class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></div>
                                <div class="edugate-content"><a href="news-detail.html" class="title">Effective researching method</a>

                                    <div class="info">
                                        <div class="author item"><a href="#">By Admin</a></div>
                                        <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                    </div>
                                    <div class="info-more">
                                        <div class="view item"><i class="fa fa-user"></i>

                                            <p> 56</p></div>
                                        <div class="comment item"><i class="fa fa-comment"></i>

                                            <p> 239</p></div>
                                    </div>
                                    <div class="description">Dalmatian hello amazing the rmore flung as thanks a manta dealt to under emu some the and one baldbe dear sobbingly save and spitefully less Dalmatian hello amazing the rmore flung as thanks a manta dealt to under emu some the and one baldbe dear sobbingly save and spitefully Dalmatian hello amazing the...</div>
                                    <button onclick="window.location.href='news-detail.html'" class="btn btn-green"><span>learn now</span></button>
                                </div>
                            </div>
                            <div class="edugate-layout-1">
                                <div class="edugate-image"><img src="assets/images/news/news-2.jpg" alt="" class="img-responsive"/></div>
                                <div class="edugate-content"><a href="news-detail.html" class="title">Effective researching method</a>

                                    <div class="info">
                                        <div class="author item"><a href="#">By Admin</a></div>
                                        <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                    </div>
                                    <div class="info-more">
                                        <div class="view item"><i class="fa fa-user"></i>

                                            <p> 56</p></div>
                                        <div class="comment item"><i class="fa fa-comment"></i>

                                            <p> 239</p></div>
                                    </div>
                                    <div class="description">Dalmatian hello amazing the rmore flung as thanks a manta dealt to under emu some the and one baldbe dear sobbingly save and spitefully less Dalmatian hello amazing the rmore flung as thanks a manta dealt to under emu some the and one baldbe dear sobbingly save and spitefully Dalmatian hello amazing the...</div>
                                    <button onclick="window.location.href='news-detail.html'" class="btn btn-green"><span>learn now</span></button>
                                </div>
                            </div>
                            <div class="edugate-layout-1">
                                <div class="edugate-image"><img src="assets/images/news/news-3.jpg" alt="" class="img-responsive"/></div>
                                <div class="edugate-content"><a href="news-detail.html" class="title">Effective researching method</a>

                                    <div class="info">
                                        <div class="author item"><a href="#">By Admin</a></div>
                                        <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                    </div>
                                    <div class="info-more">
                                        <div class="view item"><i class="fa fa-user"></i>

                                            <p> 56</p></div>
                                        <div class="comment item"><i class="fa fa-comment"></i>

                                            <p> 239</p></div>
                                    </div>
                                    <div class="description">Dalmatian hello amazing the rmore flung as thanks a manta dealt to under emu some the and one baldbe dear sobbingly save and spitefully less Dalmatian hello amazing the rmore flung as thanks a manta dealt to under emu some the and one baldbe dear sobbingly save and spitefully Dalmatian hello amazing the...</div>
                                    <button onclick="window.location.href='news-detail.html'" class="btn btn-green"><span>learn now</span></button>
                                </div>
                            </div>
                            <button class="btn btn-green btn-latest-new"><span>Browser All Post<i class="fa fa-long-arrow-right"></i></span></button>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->

@endsection
