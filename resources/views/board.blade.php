@extends('layouts.headerfooter')
@section('content')
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content">
                <div class="section background-opacity page-title set-height-top">
                    <div class="container">
                        <div class="page-title-wrapper"><h2 class="captions">Board Of Management</h2>
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li class="active"><a href="board.html">Board Of Management</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
               <style>
                .edugate-layout-3 {
    width: 100%;
    min-height: 70px;
                </style>

                <!-- OUR EVENT-->
                <div class="section section-padding events-grid">
                    <div class="container">
                        <div class="group-title-index">

                            <h2 class="center-title">The Board</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-a-01-01"></i></div>
                        </div>
                        <div class="news-page-wrapper">
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mr Stephen Muirui</a>

                                                <div class="info">
                                                    <div class="author item">Chairperson committee</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mr Edward Kirimi</a>

                                                <div class="info">
                                                    <div class="author item">Committee member </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                             <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mr Oyori Abuga</a>

                                                <div class="info">
                                                    <div class="author item">PTA REP</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                             <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mrs Florence Mugo</a>

                                                <div class="info">
                                                    <div class="author item">PTA REP</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mrs Jecinta Wanjiku</a>

                                                <div class="info">
                                                    <div class="author item">PTA REP</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mr Patrick Mbugua</a>

                                                <div class="info">
                                                    <div class="author item">PTA REP</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mrs Judy Cheruiyot</a>

                                                <div class="info">
                                                    <div class="author item">PTA REP</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">Mr Stephen Ndungu</a>

                                                <div class="info">
                                                    <div class="author item">Deputy Principal</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4 col-sm-6">
                                    <div class="edugate-layout-3 news-gird">
                                        <div class="edugate-layout-3-wrapper"><a href="#" class="edugate-image"><img src="assets/images/news/news-1.jpg" alt="" class="img-responsive"/></a>

                                            <div class="edugate-content"><a href="news-detail.html" class="title">
Mrs Priscah Ndungu                                                </a>

                                                <div class="info">
                                                    <div class="author item">Principal</div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
@endsection
